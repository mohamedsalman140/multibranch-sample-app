# Multibranch Sample App

This is a sample application demonstrating the use of Jenkins multibranch pipelines for automated CI/CD.

## Overview

The project is structured with multiple branches, each representing a different feature, fix, or development effort.

## Branches

- **main**: The main branch, considered stable and deployable.
- **dev-456:  branch for the some feature under development.
- **fix-123: Branch to fix the bug #123.

## Jenkins Multibranch Pipeline

This project utilizes Jenkins multibranch pipelines to automate the build and test process based on different branches.

### Pipeline Structure

- **Build**: The build stage compiles the code and generates artifacts.
- **Test**: The test stage runs automated tests to ensure code quality.
- **Deploy (Optional)**: Deploy stage can be added for deployment on success.


